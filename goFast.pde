/***** VARIABLES *****/

//Rectangle variables
float rectX;
float rectY;
int rectWidth = 40;
int rectHeight = 40;
color rectColor = color(255, 50, 90);

//Food variables
float foodX;
float foodY;
int foodWidth = 10;
int foodHeight = 10;
color foodColor = color(20, 200, 70);

//Game variables
boolean uCanMove;
float speed = 5;
int score;
color scoreColor = color(0, 102, 255, 75);
int time = 60 * 20;
int thig = 1;


/***** SETUP BLOCK *****/

void setup(){
  size(500, 500);

  rectX = width/2;
  rectY = height/2;

  foodX = random(0+(rectWidth/2) , width - (rectWidth/2));
  foodY = random(0+(rectHeight/2), height - (rectHeight/2));

  uCanMove = true;
  score = 0;

}

/***** DRAW BLOCK *****/
void draw() {
  //Efface l'écran
  background(255);

  //Dessine un rectangle
  drawRect();
  //Dessine la nourriture
  drawFood();
  //Affiche le score
  drawScore(score);
  //Affiche le temps
  time = time - thig;
  drawTime(time);
  //Mouvments du rectangle
  if(uCanMove) {
    if(keyPressed == true) {
      if(keyCode == UP) {
        moveUp(); 
      }
      if(keyCode == DOWN) {
        moveDown(); 
      }
      if(keyCode == LEFT) {
        moveLeft(); 
      }
      if(keyCode == RIGHT) {
        moveRight(); 
      }
    }
  }
  if(isEaten()){
    foodX = random(0+(rectWidth/2) , width - (rectWidth/2));
    foodY = random(0+(rectHeight/2), height - (rectHeight/2));
    score ++;
  }
  if(time == 0) {
    stopTime(); 
  }

}

/***** FUNCTIONS *****/

//Rectangle functions
void drawRect(){
  fill(rectColor);
  noStroke();
  rectMode(CENTER);
  rect(rectX, rectY, rectWidth, rectHeight);
}

//Food Functions
void drawFood(){
  fill(foodColor);
  noStroke();
  rectMode(CENTER);
  rect(foodX, foodY, foodWidth, foodHeight);
}

boolean isEaten() {
  if(foodX >= (rectX - (rectWidth/2)) && foodX <= (rectX + (rectWidth/2)) && foodY <= (rectY + (rectHeight/2)) && foodY >= (rectY -(rectHeight/2))) {
    return true; 
  }
  return false;
}

//Movement functions
void moveUp() {
  rectY -= speed;
  if(rectY < 0 + (rectHeight/2)) {
    rectY += speed; 
  }
}
void moveDown() {
  rectY += speed;
  if(rectY > height - (rectHeight/2)) {
    rectY -= speed; 
  }
}
void moveLeft() {
  rectX -= speed;
  if(rectX < 0 + (rectWidth/2)) {
    rectX += speed; 
  }
}
void moveRight() {
  rectX += speed;
  if(rectX > width - (rectWidth/2)) {
    rectX -= speed; 
  }
}

//Score and time functions
void drawScore(int score) {
  fill(scoreColor);
  textSize(42);
  text(score, width/2, height/2);
}
void drawTime(int time) {
  fill(scoreColor);
  textSize(30);
  text(time, width/2, (height/2) - 50);
}
void stopTime() {
  uCanMove = false;
  thig = 0;
}

/***** INPUTS *****/

void mouseClicked(){
  time = 60 * 20;
  thig = 1;
  score = 0;
  uCanMove = true;
}
